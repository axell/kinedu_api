require "application_system_test_case"

class ActivityLogsTest < ApplicationSystemTestCase
  setup do
    @activity_log = activity_logs(:one)
  end

  test "visiting the index" do
    visit activity_logs_url
    assert_selector "h1", text: "Activity Logs"
  end

  test "creating a Activity log" do
    visit activity_logs_url
    click_on "New Activity Log"

    fill_in "Activity", with: @activity_log.activity_id
    fill_in "Assistant", with: @activity_log.assistant_id
    fill_in "Baby", with: @activity_log.baby_id
    fill_in "Comments", with: @activity_log.comments
    fill_in "Duration", with: @activity_log.duration
    fill_in "Start time", with: @activity_log.start_time
    fill_in "Stop time", with: @activity_log.stop_time
    click_on "Create Activity log"

    assert_text "Activity log was successfully created"
    click_on "Back"
  end

  test "updating a Activity log" do
    visit activity_logs_url
    click_on "Edit", match: :first

    fill_in "Activity", with: @activity_log.activity_id
    fill_in "Assistant", with: @activity_log.assistant_id
    fill_in "Baby", with: @activity_log.baby_id
    fill_in "Comments", with: @activity_log.comments
    fill_in "Duration", with: @activity_log.duration
    fill_in "Start time", with: @activity_log.start_time
    fill_in "Stop time", with: @activity_log.stop_time
    click_on "Update Activity log"

    assert_text "Activity log was successfully updated"
    click_on "Back"
  end

  test "destroying a Activity log" do
    visit activity_logs_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Activity log was successfully destroyed"
  end
end
