Rails.application.routes.draw do
  
  devise_for :users
  #authenticated routes
  authenticated :user do
    resources :babies do
      resources :activity_logs 
    end
    resources :assistants
    resources :activities
    root to: "babies#index"
  end
  #unauthenticated routes
  unauthenticated :user do
    resources :babies,only:[:index] do
      resources :activity_logs,only:[:index,:create,:update]
    end
    resources :activities,only:[:index]
  end
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
end
