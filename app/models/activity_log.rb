# == Schema Information
#
# Table name: activity_logs
#
#  id           :integer          not null, primary key
#  comments     :text(65535)
#  duration     :integer
#  start_time   :datetime
#  stop_time    :datetime
#  created_at   :datetime         not null
#  updated_at   :datetime         not null
#  activity_id  :integer
#  assistant_id :integer
#  baby_id      :integer
#
# Indexes
#
#  index_activity_logs_on_activity_id   (activity_id)
#  index_activity_logs_on_assistant_id  (assistant_id)
#  index_activity_logs_on_baby_id       (baby_id)
#
# Foreign Keys
#
#  fk_rails_...  (activity_id => activities.id)
#  fk_rails_...  (assistant_id => assistants.id)
#  fk_rails_...  (baby_id => babies.id)
#
class ActivityLog < ApplicationRecord
  belongs_to :baby
  belongs_to :assistant
  belongs_to :activity

  validates :comments,:duration, :stop_time,presence:true , on: :update
  validates :start_time,presence:true, on: :create

  #validate  start_time less to stop_time
  validate :stop_time_greater_than_start_time ,on: :update

  def default_duration
    self.duration ||= 0
  end
  
  #set duration of activity
  def set_duration
    time = stop_time - start_time
    time = (time / 1.minute).round
    self.update(duration: time)
  end

  #get the name of assistant
  def assistant_name
    assistant.name
  end

  #check if the activity was finalized
  def activity_finished_at
    time = stop_time - start_time if stop_time
    mins = (time / 1.minute).round if time
    mins == duration ? stop_time : stop_time = "En progreso" if mins
  end

  #config validation if start_time less to stop_time

  def stop_time_greater_than_start_time
    if  start_time > stop_time
      errors.add(:stop_time,"no puede ser menor a la fecha y hora de inicio ")
    end
  end

end
