class ActivityLogsController < ApplicationController
  before_action :set_activity_log, only: [:show, :edit, :update, :destroy]
  before_action :set_baby
  # GET /activity_logs
  # GET /activity_logs.json
  def index
    @activity_logs = @baby.activity_logs.all.order(baby_id: :asc)
  end

  # GET /activity_logs/1
  # GET /activity_logs/1.json
  def show
  end

  # GET /activity_logs/new
  def new
    @activity_log = ActivityLog.new
  end

  # GET /activity_logs/1/edit
  def edit
  end

  # POST /activity_logs
  # POST /activity_logs.json
  def create
    
    @activity_log = @baby.activity_logs.new(activity_log_params)
    respond_to do |format|
      if @activity_log.save
        format.html { redirect_to baby_activity_logs_path, notice: 'Activity log was successfully created.' }
        format.json { render :index, status: :created, location: baby_activity_logs_path }
      else
        format.html { render :new }
        format.json { render json: @activity_log.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /activity_logs/1
  # PATCH/PUT /activity_logs/1.json
  def update
    respond_to do |format|

      @activity_log.default_duration
      if @activity_log.update(activity_log_params)
         @activity_log.set_duration
        format.html { redirect_to @activity_log, notice: 'Activity log was successfully updated.' }
        format.json { render :index, status: :ok, location: baby_activity_logs_path }
      else
        format.html { render :edit }
        format.json { render json: @activity_log.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /activity_logs/1
  # DELETE /activity_logs/1.json
  def destroy
    @activity_log.destroy
    respond_to do |format|
      format.html { redirect_to activity_logs_url, notice: 'Activity log was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_activity_log
      @activity_log = ActivityLog.find(params[:id])
    end
    def set_baby
      @baby = Baby.find(params[:baby_id])
    end

    # Only allow a list of trusted parameters through.
    def activity_log_params
      params.require(:activity_log).permit(:baby_id, :assistant_id, :activity_id, :start_time, :stop_time, :duration, :comments)
    end
end
