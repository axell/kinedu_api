class ApplicationController < ActionController::Base
  #skip authenticity token for data sended with res api 
  skip_before_action :verify_authenticity_token
  #validate that users are authenticated
  before_action :authenticate_user!
end
